/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import teste.DoceDeLeiteApplicationTests;

/**
 *
 * @author douglas
 */
public class GastosControllerTest extends DoceDeLeiteApplicationTests {

    @Autowired
    public GastosController gastosController;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @After
    public void tearDown() {
    }

    @Override
    protected AbstractRestController getController() {
        return gastosController;
    }
    
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testSomaEstados() throws Exception {
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/gastos/somaestado?inicio=01/2018&fim=02/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].sigla").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].valor").isNumber());
    }
    
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testSomaPeriodo() throws Exception {
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/gastos/somaperiodo?inicio=01/2018&fim=02/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.soma").isNumber());
    }
    
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testMediaPeriodo() throws Exception {
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/gastos/mediaperiodo?inicio=01/2018&fim=02/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.soma").isNumber());
    }

    
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testToken() throws Exception {
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/gastos/token"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
    
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testMaiorGasto() throws Exception {
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/gastos/maior?inicio=01/2018&fim=02/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.valor").isNumber());
    }
}
