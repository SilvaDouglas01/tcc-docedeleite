/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;


import br.com.dbc.docedeleite.repository.EstabelecimentoRepository;
import br.com.dbc.docedeleite.repository.EstadoRepository;
import br.com.dbc.docedeleite.repository.GastoRepository;
import br.com.dbc.docedeleite.repository.GrupoRepository;
import br.com.dbc.docedeleite.repository.MunicipioRepository;
import br.com.dbc.docedeleite.repository.OrgaoMaximoRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import teste.DoceDeLeiteApplicationTests;

/**
 *
 * @author jonas
 */
public class OrgaoMaximoRestControllerTest extends DoceDeLeiteApplicationTests {

    @Autowired
    private GastoRepository gastoRepository;

    @Autowired
    private OrgaoMaximoRestController orgaoMaximoRestController;

    @Autowired
    private EstabelecimentoRepository estabRepository;

    @Autowired
    private GrupoRepository grupoRepository;

    @Autowired
    private MunicipioRepository munRepository;

    @Autowired
    private OrgaoMaximoRepository orgaoRepository;

    @Autowired
    private EstadoRepository estadoRepository;

    public OrgaoMaximoRestControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @After
    public void tearDown() {
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testGraficoBolhas() throws Exception {
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/orgaos/graficobolhas?dataInicio=01/2018&dataFim=02/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].nome").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].valor").isNumber());
    }

    @Override
    protected AbstractRestController getController() {
        return orgaoMaximoRestController;
    }

}