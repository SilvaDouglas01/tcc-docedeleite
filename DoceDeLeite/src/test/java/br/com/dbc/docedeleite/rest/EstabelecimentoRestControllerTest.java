/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import teste.DoceDeLeiteApplicationTests;

/**
 *
 * @author jonas
 */
public class EstabelecimentoRestControllerTest extends DoceDeLeiteApplicationTests {
    
    @Autowired
    private EstabelecimentoRestController estabRestController;
    
    public EstabelecimentoRestControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getService method, of class EstabelecimentoRestController.
     */
    @Test
    public void testGetService() {
       
    }

    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testGetNomeEValorTotal() throws Exception {
       restMockMvc.perform(MockMvcRequestBuilders.get("/api/estabelecimento/valorTotal?nome=Sem informação&dataInicio=01/2018&dataFim=02/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].nome").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].valor").isNumber());
    }

    @Override
    protected AbstractRestController getController() {
        return estabRestController;
    }
    
}
