/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import br.com.dbc.docedeleite.repository.GrupoRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import teste.DoceDeLeiteApplicationTests;

/**
 *
 * @author douglas
 */
public class GrupoRestControllerTest extends DoceDeLeiteApplicationTests {
       
    @Autowired
    private GrupoRestController grupoRestController;


    public GrupoRestControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @After
    public void tearDown() {
    }

    @Override
    protected AbstractRestController getController() {
        return grupoRestController;
    }
    
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testTabelacao() throws Exception {
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/grupo/tabela/0/20?dataInicio=02/2018&dataFim=03/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].nome").isString())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].valor").isNumber());
    }
    
    @WithMockUser(username = "admin.admin",
            password = "jwtpass",
            authorities = {"ADMIN_USER"})
    @Test
    public void testGetPagesTabelacao() throws Exception {
        
        restMockMvc.perform(MockMvcRequestBuilders.get("/api/grupo/tamanho/tabela?dataInicio=02/2018&dataFim=03/2018"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.soma").isNumber());
    }

}
