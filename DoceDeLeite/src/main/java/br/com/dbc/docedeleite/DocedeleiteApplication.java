package br.com.dbc.docedeleite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocedeleiteApplication {


    public static void main(String[] args) {
        SpringApplication.run(DocedeleiteApplication.class, args);
    }
}
