/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.entity;

import br.com.dbc.docedeleite.dto.GrupoTabelaDTO;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "GRUPO")
@SequenceGenerator(name = "GRUPO_SEQ", sequenceName = "GRUPO_SEQ", allocationSize = 1)
@SqlResultSetMapping(
        name = "getResult",
        classes = {
            @ConstructorResult(
                    targetClass = GrupoTabelaDTO.class,
                    columns = {
                        @ColumnResult(name = "nome", type = String.class)
                        ,
                               @ColumnResult(name = "valor", type = Double.class)
                    }
            )
        })
@NamedNativeQuery(
        
        name = "Grupo.valorTotal",
        
        query = "select grupo.nome, sum(v.valor_transacao) as valor\n"
        + "from gasto v \n"
        + "inner join estabelecimento on v.id_estabelecimento = estabelecimento.id\n"
        + "inner join grupo on estabelecimento.id_grupo = grupo.id\n"
        + "where v.mes_extrato between :inicio and :fim \n"        
        + "group by grupo.nome order by valor desc ",
        resultSetMapping = "getResult"
)
public class Grupo extends AbstractEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GRUPO_SEQ")
    private Long id;

    @NotNull
    @Column(name = "NOME")
    private String nome;
}
