package br.com.dbc.docedeleite.service;

import br.com.dbc.docedeleite.dto.EstabelecimentoValorDTO;
import br.com.dbc.docedeleite.dto.SomaDTO;
import br.com.dbc.docedeleite.rest.dto.EstabelecimentoDTO;
import br.com.dbc.docedeleite.entity.Estabelecimento;
import br.com.dbc.docedeleite.repository.EstabelecimentoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class EstabelecimentoService extends AbstractCrudService<Estabelecimento> {

    @Autowired
    private EstabelecimentoRepository repository;

    @Autowired
    private GrupoService grupoService;

    @Override
    public JpaRepository getRepository() {
        return repository;
    }

    public Estabelecimento salvarDeDTO(EstabelecimentoDTO dto) {

        Estabelecimento estab = repository.findByNome(dto.getNome());

        if (estab == null) {
            estab = repository.save(
                    Estabelecimento.builder()
                            .nome(dto.getNome())
                            .grupo(grupoService.salvarDeDTO(dto.getCnae()))
                            .build());
        }

        return estab;
    }

    public List<EstabelecimentoValorDTO> getNomesAndValorTotal(String nome, String dataIni, String dataFim) {

        List<EstabelecimentoValorDTO> estabs = repository.valorTotalEstabelecimento(nome, "01/" + dataIni,
                "01/" + dataFim);

        return estabs;
    }

    /*public SomaDTO getPageCountTabelaEstabelecimento(String nome,String dataIni, String dataFim) {
        return SomaDTO.builder()
                .soma(getNomesAndValorTotal(0l, 20l, nome, dataIni, dataFim).getPageCount() * 1.0)
                .build();
    }*/
}
