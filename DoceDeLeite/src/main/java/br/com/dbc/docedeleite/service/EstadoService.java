package br.com.dbc.docedeleite.service;

import br.com.dbc.docedeleite.dto.ValorEstadoDTO;
import br.com.dbc.docedeleite.rest.dto.UfDTO;
import br.com.dbc.docedeleite.entity.Estado;
import br.com.dbc.docedeleite.repository.EstadoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class EstadoService extends AbstractCrudService<Estado> {

    @Autowired
    private EstadoRepository repository;

    @Override
    public JpaRepository getRepository() {
        return repository;
    }

    public Estado salvarDeDTO(UfDTO dto) {

        Estado estado = repository.findByNome(dto.getNome());

        if (estado == null) {
            estado = repository.save(Estado
                    .builder()
                    .nome(dto.getNome())
                    .sigla(dto.getSigla())
                    .build());
        }

        return estado;
    }
    
    public List<ValorEstadoDTO> getSiglaEValorTotal(String dataIni, String dataFim){
        return repository.getSiglaEValorTotal(dataIni, dataFim);
    }

}
