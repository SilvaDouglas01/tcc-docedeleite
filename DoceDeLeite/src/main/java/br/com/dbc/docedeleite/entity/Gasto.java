/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.borges
 */
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "GASTO")
@SequenceGenerator(name = "GASTO_SEQ", sequenceName = "GASTO_SEQ", allocationSize = 1)
public class Gasto extends AbstractEntity implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GASTO_SEQ" )
    private Long id;
    
    @NotNull
    @Column(name = "ID_GASTO_REST")
    private Long idGastoRest;

    @NotNull
    @Column(name = "MES_EXTRATO")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate mesExtrato;

    @Column(name = "DATA_TRANSACAO")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate dataTransacao;

    @NotNull
    @Column(name = "VALOR_TRANSACAO")
    private Double valorTransacao;

    
    @JoinColumn(name = "ID_ESTABELECIMENTO", referencedColumnName = "ID")
    @ManyToOne
    private Estabelecimento estabelecimento;

    
    @JoinColumn(name = "ID_MUNICIPIO", referencedColumnName = "ID")
    @ManyToOne
    private Municipio municipio;

    @JoinColumn(name = "ID_ORGAO_MAXIMO", referencedColumnName = "ID")
    @ManyToOne
    private OrgaoMaximo orgaoMaximo;
}
