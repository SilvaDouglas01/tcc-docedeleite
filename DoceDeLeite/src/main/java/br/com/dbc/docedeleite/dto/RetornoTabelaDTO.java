/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RetornoTabelaDTO {

    private String nome;

    private Double valor;

    public RetornoTabelaDTO toRetornoTabela(GrupoTabelaDTO grupo) {
        this.setNome(grupo.getNome());
        this.setValor(grupo.getValor());
        return this;
    }

}
