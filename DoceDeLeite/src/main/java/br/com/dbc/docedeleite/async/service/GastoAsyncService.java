/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.async.service;

import br.com.dbc.docedeleite.entity.ControleProcesso;
import br.com.dbc.docedeleite.enums.Status;
import br.com.dbc.docedeleite.service.ControleProcessoService;
import br.com.dbc.docedeleite.service.GastoService;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jonas
 */
@Service
public class GastoAsyncService {

    @Autowired
    private GastoService gastoService;

    @Autowired
    private ControleProcessoService controleProcessoService;

    public void geMultiplasPaginas() {

        for (int i = 12000; i < 20994; i++) {
            ControleProcesso p = controleProcessoService.save(ControleProcesso.builder()
                    .paginaAtual(i)
                    .dataInicio(LocalDateTime.now())
                    .status(Status.INICIO).build());
            gastoService.buscarTransacoes(i, p);

        }
    }

}
