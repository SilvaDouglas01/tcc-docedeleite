/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import br.com.dbc.docedeleite.entity.User;
import br.com.dbc.docedeleite.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author jonas
 */
@RestController
@RequestMapping("/api/user")
public class UserRestController extends AbstractRestController<User, UserServiceImpl> {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @PostMapping("/create")
    public ResponseEntity<?> novoUsuario(@RequestBody User user) throws Exception {
        return ResponseEntity.ok(userServiceImpl.novoUsuario(user));
    }

    @PostMapping("/password")
    public ResponseEntity<?> updatePassword(@RequestBody User user) {
        return ResponseEntity.ok(userServiceImpl.updatePassword(user));
    }

    @Override
    protected UserServiceImpl getService() {
        return userServiceImpl;
    }

    @GetMapping("/details")
    @ResponseBody
    public User currentUserName(Authentication authentication) {
        User user = userServiceImpl.getUserDetails(authentication.getName());
        return user;
    }
}
