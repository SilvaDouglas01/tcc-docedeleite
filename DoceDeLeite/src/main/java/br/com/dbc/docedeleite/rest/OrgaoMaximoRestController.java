/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import br.com.dbc.docedeleite.entity.OrgaoMaximo;
import br.com.dbc.docedeleite.service.OrgaoMaximoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jonas
 */
@RestController
@RequestMapping("/api/orgaos")
public class OrgaoMaximoRestController extends AbstractRestController<OrgaoMaximo, OrgaoMaximoService> {

    @Autowired
    private OrgaoMaximoService orgaoMaximoService;

    @Override
    protected OrgaoMaximoService getService() {
        return orgaoMaximoService;
    }

    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @GetMapping("/graficobolhas")
    public ResponseEntity<?> graficoBolhas(@RequestParam String dataInicio, @RequestParam String dataFim){
        return ResponseEntity.ok(orgaoMaximoService.getGraficoBolhas(dataInicio, dataFim));
    }
}
