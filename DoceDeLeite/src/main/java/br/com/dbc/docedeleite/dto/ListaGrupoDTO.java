/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author gustavo.comaru
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ListaGrupoDTO {
    private String nome;
    private Double valor;
    private List<ListaEstabelecimentoDTO> estabelecimentos;
}
