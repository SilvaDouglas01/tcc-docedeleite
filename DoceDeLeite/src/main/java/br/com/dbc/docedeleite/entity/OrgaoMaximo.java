/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.entity;

import br.com.dbc.docedeleite.dto.GraficoBolhasDTO;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.borges
 */
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ORGAOMAXIMO")
@SequenceGenerator(name = "ORGAO_MAXIMO_SEQ", sequenceName = "ORGAO_MAXIMO_SEQ", allocationSize = 1)
@SqlResultSetMapping(
        name = "getOrgaoResult",
        classes = {
            @ConstructorResult(
                    targetClass = GraficoBolhasDTO.class,
                    columns = {
                        @ColumnResult(name = "nome", type = String.class)
                        ,
                               @ColumnResult(name = "valor", type = Double.class)
                    }
            )
        })
@NamedNativeQuery(
        name = "OrgaoMaximo.findValorTransferenciaByOrgaoMaximoNome",
        query = "select o.nome, sum(g.valor_transacao) as valor\n"
        + "from orgaomaximo o\n"
        + "inner join gasto g on g.id_orgao_maximo = o.id\n"
        + "and g.mes_extrato between :ini and :fim\n"
        + "group by o.nome order by valor desc",
        resultSetMapping = "getOrgaoResult"
)
public class OrgaoMaximo extends AbstractEntity implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORGAO_MAXIMO_SEQ")
    private Long id;

    @Column(name = "SIGLA")
    @Size(min = 1, max = 100)
    @NotNull
    private String sigla;

    @Column(name = "NOME")
    @Size(min = 1, max = 100)
    @NotNull
    private String nome;
}
