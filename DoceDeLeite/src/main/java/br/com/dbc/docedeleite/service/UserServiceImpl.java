/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.service;

import br.com.dbc.docedeleite.configurations.EmailValidator;
import br.com.dbc.docedeleite.entity.User;
import br.com.dbc.docedeleite.repository.RoleRepository;
import br.com.dbc.docedeleite.repository.UserRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jonas
 */
@Service
public class UserServiceImpl extends AbstractCrudService<User> implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Transactional
    public User novoUsuario(User user) throws Exception {
        
        boolean emailValido = new EmailValidator().validateEmail(user.getEmail());
        
        if(!emailValido){
            throw new Exception(String.format("The email %s is not valid", user.getEmail()));
        }
        
        User userValidator = userRepository.findByUsername(user.getUsername());

        if (userValidator != null) {
            throw new UsernameNotFoundException(String.format("The username %s already exists", userValidator));
        }
        
        userValidator = userRepository.findByEmail(user.getEmail());
        
        if (userValidator != null) {
            throw new Exception(String.format("The email %s already exists", userValidator.getEmail()));
        }
        
        user.setRoles(Arrays.asList(roleRepository.findByRoleName("STANDARD_USER")));
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public User updatePassword(User user) {
        User userParaAtualizar = userRepository.findByUsername(user.getUsername());
        if (userParaAtualizar == null) {
            return null;
        }
        userParaAtualizar.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(userParaAtualizar);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(s);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }

        List<GrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
        });

        UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(), authorities);

        return userDetails;
    }

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }
    
    public User getUserDetails(String nome){
        User user = userRepository.findByUsername(nome);
        return user;
    }

}
