/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.repository;

import br.com.dbc.docedeleite.dto.EstabelecimentoValorDTO;
import br.com.dbc.docedeleite.entity.Estabelecimento;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author gustavo.comaru
 */
public interface EstabelecimentoRepository extends JpaRepository<Estabelecimento, Long> {

    public Estabelecimento findByNome(String nome);

    @Query(nativeQuery = true)
    public List<EstabelecimentoValorDTO> valorTotalEstabelecimento(@Param("nome") String nome,
            @Param("inicio") String inicio,
             @Param("fim") String fim);
}
