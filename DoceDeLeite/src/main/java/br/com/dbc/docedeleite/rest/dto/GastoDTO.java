
package br.com.dbc.docedeleite.rest.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dataTransacao",
    "estabelecimento",
    "id",
    "mesExtrato",
    "portador",
    "tipoCartao",
    "unidadeGestora",
    "valorTransacao"
})
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GastoDTO {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    @JsonProperty("dataTransacao")
    private LocalDate dataTransacao;
    @JsonProperty("estabelecimento")
    private EstabelecimentoDTO estabelecimento;
    @JsonProperty("id")
    private Long id;
    @JsonProperty("mesExtrato")
    private String mesExtrato;
    @JsonProperty("portador")
    private PortadorDTO portador;
    @JsonProperty("tipoCartao")
    private TipoCartaoDTO tipoCartao;
    @JsonProperty("unidadeGestora")
    private UnidadeGestoraDTO unidadeGestora;
    @JsonProperty("valorTransacao")
    private String valorTransacao;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("dataTransacao")
    public LocalDate getDataTransacao() {
        return dataTransacao;
    }

    @JsonProperty("dataTransacao")
    public void setDataTransacao(LocalDate dataTransacao) {
        this.dataTransacao = dataTransacao;
    }

    @JsonProperty("estabelecimento")
    public EstabelecimentoDTO getEstabelecimento() {
        return estabelecimento;
    }

    @JsonProperty("estabelecimento")
    public void setEstabelecimento(EstabelecimentoDTO estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("mesExtrato")
    public String getMesExtrato() {
        return mesExtrato;
    }

    @JsonProperty("mesExtrato")
    public void setMesExtrato(String mesExtrato) {
        this.mesExtrato = mesExtrato;
    }

    @JsonProperty("portador")
    public PortadorDTO getPortador() {
        return portador;
    }

    @JsonProperty("portador")
    public void setPortador(PortadorDTO portador) {
        this.portador = portador;
    }

    @JsonProperty("tipoCartao")
    public TipoCartaoDTO getTipoCartao() {
        return tipoCartao;
    }

    @JsonProperty("tipoCartao")
    public void setTipoCartao(TipoCartaoDTO tipoCartao) {
        this.tipoCartao = tipoCartao;
    }

    @JsonProperty("unidadeGestora")
    public UnidadeGestoraDTO getUnidadeGestora() {
        return unidadeGestora;
    }

    @JsonProperty("unidadeGestora")
    public void setUnidadeGestora(UnidadeGestoraDTO unidadeGestora) {
        this.unidadeGestora = unidadeGestora;
    }

    @JsonProperty("valorTransacao")
    public String getValorTransacao() {
        return valorTransacao;
    }

    @JsonProperty("valorTransacao")
    public void setValorTransacao(String valorTransacao) {
        this.valorTransacao = valorTransacao;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
