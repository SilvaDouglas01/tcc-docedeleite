/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.repository;

import br.com.dbc.docedeleite.entity.Gasto;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 *
 * @author gustavo.comaru
 */
public interface GastoRepository extends JpaRepository<Gasto, Long>{
    
    public Gasto findByIdGastoRest(Long id);
    
    @Query("SELECT sum(u.valorTransacao) FROM Gasto u WHERE u.municipio.estado.sigla = :sigla and u.mesExtrato between :dataInicio and :dataFim")
    public Double findGastoByMunicipioEstadoSigla(@Param("sigla") String Sigla, @Param("dataInicio") LocalDate dataInicio, @Param("dataFim") LocalDate dataFim);
    
    @Query("SELECT sum(u.valorTransacao) FROM Gasto u WHERE u.mesExtrato between :dataInicio and :dataFim")
    public Double sumValorByLocalDateBetween(@Param("dataInicio") LocalDate dataInicio, @Param("dataFim") LocalDate dataFim);
    
    public List<Gasto> findByMesExtratoBetween(LocalDate ini, LocalDate fin);
    
    public Long countByMesExtratoBetween(LocalDate inicio, LocalDate fim);
    
    public Gasto findTopByMesExtratoBetweenOrderByValorTransacaoDesc(LocalDate inicio, LocalDate fim);
    
}
