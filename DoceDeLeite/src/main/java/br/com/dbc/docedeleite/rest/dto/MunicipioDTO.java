
package br.com.dbc.docedeleite.rest.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "codigoIBGE",
    "nomeIBGE",
    "pais",
    "uf"
})
public class MunicipioDTO {

    @JsonProperty("codigoIBGE")
    private String codigoIBGE;
    @JsonProperty("nomeIBGE")
    private String nomeIBGE;
    @JsonProperty("pais")
    private String pais;
    @JsonProperty("uf")
    private UfDTO uf;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("codigoIBGE")
    public String getCodigoIBGE() {
        return codigoIBGE;
    }

    @JsonProperty("codigoIBGE")
    public void setCodigoIBGE(String codigoIBGE) {
        this.codigoIBGE = codigoIBGE;
    }

    @JsonProperty("nomeIBGE")
    public String getNomeIBGE() {
        return nomeIBGE;
    }

    @JsonProperty("nomeIBGE")
    public void setNomeIBGE(String nomeIBGE) {
        this.nomeIBGE = nomeIBGE;
    }

    @JsonProperty("pais")
    public String getPais() {
        return pais;
    }

    @JsonProperty("pais")
    public void setPais(String pais) {
        this.pais = pais;
    }

    @JsonProperty("uf")
    public UfDTO getUf() {
        return uf;
    }

    @JsonProperty("uf")
    public void setUf(UfDTO uf) {
        this.uf = uf;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
