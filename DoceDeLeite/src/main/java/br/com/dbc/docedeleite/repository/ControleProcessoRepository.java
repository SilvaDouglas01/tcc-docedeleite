/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.repository;

import br.com.dbc.docedeleite.entity.ControleProcesso;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jonas
 */
public interface ControleProcessoRepository extends JpaRepository<ControleProcesso, Long> {
    
}
