/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.enums;

/**
 *
 * @author douglas
 */
public enum SiglaEstado {
    AC, AL, AP, AM, BA, CE, DF, ES, GO, MA, MT, MS, MG, PA, PB, PR, PE, PI, RR, RO, RJ, RN, RS, SC, SP, SE, TO;
}
