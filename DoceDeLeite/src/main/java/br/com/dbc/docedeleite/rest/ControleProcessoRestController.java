/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import br.com.dbc.docedeleite.entity.ControleProcesso;
import br.com.dbc.docedeleite.service.ControleProcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas
 */
@RestController
@RequestMapping("/api/controle")
public class ControleProcessoRestController extends AbstractRestController<ControleProcesso, ControleProcessoService> {

    @Autowired
    private ControleProcessoService controleProcessoService;

    @Override
    protected ControleProcessoService getService() {
        return controleProcessoService;
    }

}
