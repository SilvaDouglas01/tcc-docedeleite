package br.com.dbc.docedeleite.rest;

import br.com.dbc.docedeleite.async.service.GastoAsyncService;
import br.com.dbc.docedeleite.entity.Gasto;
import br.com.dbc.docedeleite.service.GastoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api/gastos")
public class GastosController  extends AbstractRestController<Gasto, GastoService>{

    @Autowired
    private GastoService gastoService;
    
    @Autowired
    private GastoAsyncService gastoAsyncService;

    @GetMapping("/baixar")
    @PreAuthorize("hasAuthority('ADMIN_USER')")
    public void buscarTransacoes() {
        gastoAsyncService.geMultiplasPaginas();
    }

    @Override
    protected GastoService getService() {
        return gastoService;
    }
    
    
    @GetMapping("/somaestado")
    public ResponseEntity<?> somaPorEstado(@RequestParam String inicio, @RequestParam String fim){
        return ResponseEntity.ok(gastoService.somaPorEstado(inicio, fim));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @GetMapping("/somaperiodo")
    public ResponseEntity<?> somaPeriodo(@RequestParam String inicio, @RequestParam String fim){
        return ResponseEntity.ok(gastoService.somaPeriodo(inicio, fim));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @GetMapping("/mediaperiodo")
    public ResponseEntity<?> mediaPeriodo(@RequestParam String inicio, @RequestParam String fim){
        return ResponseEntity.ok(gastoService.mediaPeriodo(inicio, fim));
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @GetMapping("token")
    public ResponseEntity<?> token(){
        return ResponseEntity.ok(null);
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @GetMapping("maior")
    public ResponseEntity<?> getMaiorGasto(@RequestParam String inicio, @RequestParam String fim){
        return ResponseEntity.ok(gastoService.getMaiorGastoPeriodo(inicio, fim));
    }

}
