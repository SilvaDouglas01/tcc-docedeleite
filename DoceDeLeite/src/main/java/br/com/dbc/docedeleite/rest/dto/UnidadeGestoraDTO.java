
package br.com.dbc.docedeleite.rest.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "codigo",
    "nome",
    "orgaoVinculado"
})
public class UnidadeGestoraDTO {

    @JsonProperty("codigo")
    private String codigo;
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("orgaoVinculado")
    private OrgaoVinculadoDTO orgaoVinculado;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("codigo")
    public String getCodigo() {
        return codigo;
    }

    @JsonProperty("codigo")
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @JsonProperty("nome")
    public String getNome() {
        return nome;
    }

    @JsonProperty("nome")
    public void setNome(String nome) {
        this.nome = nome;
    }

    @JsonProperty("orgaoVinculado")
    public OrgaoVinculadoDTO getOrgaoVinculado() {
        return orgaoVinculado;
    }

    @JsonProperty("orgaoVinculado")
    public void setOrgaoVinculado(OrgaoVinculadoDTO orgaoVinculado) {
        this.orgaoVinculado = orgaoVinculado;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
