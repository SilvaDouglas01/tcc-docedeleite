/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.service;

import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author douglas
 */
@Service
public abstract class AbstractCrudService<E> {
    
    @Autowired
    public abstract JpaRepository<E, Long> getRepository();
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public E save(@NotNull @Valid E entity) {
        return getRepository().save(entity);
    }
    
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete(Long id) {
        getRepository().deleteById(id);
    }
    
    public Optional<E> findById(Long id){
        return getRepository().findById(id);
    }
    
    public Page<E> findAll(Pageable pageable) {
        System.out.println("passei aqui");
        return getRepository().findAll(pageable);
    }
}
