
package br.com.dbc.docedeleite.rest.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cnpj",
    "codigoSIAFI",
    "descricaoPoder",
    "nome",
    "orgaoMaximo",
    "sigla"
})
public class OrgaoVinculadoDTO {

    @JsonProperty("cnpj")
    private String cnpj;
    @JsonProperty("codigoSIAFI")
    private String codigoSIAFI;
    @JsonProperty("descricaoPoder")
    private String descricaoPoder;
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("orgaoMaximo")
    private OrgaoMaximoDTO orgaoMaximo;
    @JsonProperty("sigla")
    private String sigla;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cnpj")
    public String getCnpj() {
        return cnpj;
    }

    @JsonProperty("cnpj")
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    @JsonProperty("codigoSIAFI")
    public String getCodigoSIAFI() {
        return codigoSIAFI;
    }

    @JsonProperty("codigoSIAFI")
    public void setCodigoSIAFI(String codigoSIAFI) {
        this.codigoSIAFI = codigoSIAFI;
    }

    @JsonProperty("descricaoPoder")
    public String getDescricaoPoder() {
        return descricaoPoder;
    }

    @JsonProperty("descricaoPoder")
    public void setDescricaoPoder(String descricaoPoder) {
        this.descricaoPoder = descricaoPoder;
    }

    @JsonProperty("nome")
    public String getNome() {
        return nome;
    }

    @JsonProperty("nome")
    public void setNome(String nome) {
        this.nome = nome;
    }

    @JsonProperty("orgaoMaximo")
    public OrgaoMaximoDTO getOrgaoMaximo() {
        return orgaoMaximo;
    }

    @JsonProperty("orgaoMaximo")
    public void setOrgaoMaximo(OrgaoMaximoDTO orgaoMaximo) {
        this.orgaoMaximo = orgaoMaximo;
    }

    @JsonProperty("sigla")
    public String getSigla() {
        return sigla;
    }

    @JsonProperty("sigla")
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
