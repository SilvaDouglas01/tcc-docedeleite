/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import br.com.dbc.docedeleite.entity.Estabelecimento;
import br.com.dbc.docedeleite.service.EstabelecimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author douglas
 */
@RestController
@RequestMapping("/api/estabelecimento")
public class EstabelecimentoRestController extends AbstractRestController<Estabelecimento, EstabelecimentoService> {
    
    @Autowired
    private EstabelecimentoService estabelecimentoService;

    @Override
    protected EstabelecimentoService getService() {
        return estabelecimentoService;
    }
    
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @GetMapping("/valorTotal")
    public ResponseEntity<?> getNomeEValorTotal(String nome, String dataInicio, String dataFim){
        return ResponseEntity.ok(getService().getNomesAndValorTotal(nome, dataInicio, dataFim));
    }
    
}
