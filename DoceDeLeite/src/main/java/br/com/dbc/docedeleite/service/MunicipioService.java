package br.com.dbc.docedeleite.service;

import br.com.dbc.docedeleite.rest.dto.MunicipioDTO;
import br.com.dbc.docedeleite.entity.Municipio;
import br.com.dbc.docedeleite.repository.MunicipioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class MunicipioService extends AbstractCrudService<Municipio> {

    @Autowired
    private MunicipioRepository repository;

    @Autowired
    private EstadoService estadoService;

    @Override
    public JpaRepository getRepository() {
        return repository;
    }

    public Municipio salvarDeDTO(MunicipioDTO dto) {

        Municipio municipio = repository.findByNomeIBGE(dto.getNomeIBGE());

        if (municipio == null) {
            municipio = repository.save(Municipio
                    .builder()
                    .nomeIBGE(dto.getNomeIBGE())
                    .estado(estadoService.salvarDeDTO(dto.getUf()))
                    .build());
        }

        return municipio;
    }

}
