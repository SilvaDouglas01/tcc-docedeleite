package br.com.dbc.docedeleite.service;

import br.com.dbc.docedeleite.dto.GraficoBolhasDTO;
import br.com.dbc.docedeleite.rest.dto.OrgaoMaximoDTO;
import br.com.dbc.docedeleite.entity.OrgaoMaximo;
import br.com.dbc.docedeleite.repository.OrgaoMaximoRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class OrgaoMaximoService extends AbstractCrudService<OrgaoMaximo> {

    @Autowired
    private OrgaoMaximoRepository repository;

    @Override
    public JpaRepository getRepository() {
        return repository;
    }

    public OrgaoMaximo salvarDeDTO(OrgaoMaximoDTO dto) {

        OrgaoMaximo orgao = repository.findByNome(dto.getNome());

        if (orgao == null) {
            orgao = repository.save(OrgaoMaximo
                    .builder()
                    .sigla(dto.getSigla())
                    .nome(dto.getNome())
                    .build());
        }

        return orgao;
    }

    public List<GraficoBolhasDTO> getGraficoBolhas(String dataInicio, String dataFim) {
       
        List<GraficoBolhasDTO> graficoRetorno = repository
                .findValorTransferenciaByOrgaoMaximoNome("01/" + dataInicio, "01/" + dataFim);
        
        graficoRetorno = graficoRetorno
            .stream()
            .limit(10)
            .collect(Collectors.toList());
        
        return graficoRetorno;
    }
}
