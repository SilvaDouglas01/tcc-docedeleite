
package br.com.dbc.docedeleite.rest.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "classe",
    "codigoClasse",
    "codigoDivisao",
    "codigoGrupo",
    "codigoSecao",
    "codigoSubclasse",
    "divisao",
    "grupo",
    "secao",
    "subclasse"
})
public class CnaeDTO {

    @JsonProperty("classe")
    private String classe;
    @JsonProperty("codigoClasse")
    private String codigoClasse;
    @JsonProperty("codigoDivisao")
    private String codigoDivisao;
    @JsonProperty("codigoGrupo")
    private String codigoGrupo;
    @JsonProperty("codigoSecao")
    private String codigoSecao;
    @JsonProperty("codigoSubclasse")
    private String codigoSubclasse;
    @JsonProperty("divisao")
    private String divisao;
    @JsonProperty("grupo")
    private String grupo;
    @JsonProperty("secao")
    private String secao;
    @JsonProperty("subclasse")
    private String subclasse;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("classe")
    public String getClasse() {
        return classe;
    }

    @JsonProperty("classe")
    public void setClasse(String classe) {
        this.classe = classe;
    }

    @JsonProperty("codigoClasse")
    public String getCodigoClasse() {
        return codigoClasse;
    }

    @JsonProperty("codigoClasse")
    public void setCodigoClasse(String codigoClasse) {
        this.codigoClasse = codigoClasse;
    }

    @JsonProperty("codigoDivisao")
    public String getCodigoDivisao() {
        return codigoDivisao;
    }

    @JsonProperty("codigoDivisao")
    public void setCodigoDivisao(String codigoDivisao) {
        this.codigoDivisao = codigoDivisao;
    }

    @JsonProperty("codigoGrupo")
    public String getCodigoGrupo() {
        return codigoGrupo;
    }

    @JsonProperty("codigoGrupo")
    public void setCodigoGrupo(String codigoGrupo) {
        this.codigoGrupo = codigoGrupo;
    }

    @JsonProperty("codigoSecao")
    public String getCodigoSecao() {
        return codigoSecao;
    }

    @JsonProperty("codigoSecao")
    public void setCodigoSecao(String codigoSecao) {
        this.codigoSecao = codigoSecao;
    }

    @JsonProperty("codigoSubclasse")
    public String getCodigoSubclasse() {
        return codigoSubclasse;
    }

    @JsonProperty("codigoSubclasse")
    public void setCodigoSubclasse(String codigoSubclasse) {
        this.codigoSubclasse = codigoSubclasse;
    }

    @JsonProperty("divisao")
    public String getDivisao() {
        return divisao;
    }

    @JsonProperty("divisao")
    public void setDivisao(String divisao) {
        this.divisao = divisao;
    }

    @JsonProperty("grupo")
    public String getGrupo() {
        return grupo;
    }

    @JsonProperty("grupo")
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    @JsonProperty("secao")
    public String getSecao() {
        return secao;
    }

    @JsonProperty("secao")
    public void setSecao(String secao) {
        this.secao = secao;
    }

    @JsonProperty("subclasse")
    public String getSubclasse() {
        return subclasse;
    }

    @JsonProperty("subclasse")
    public void setSubclasse(String subclasse) {
        this.subclasse = subclasse;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
