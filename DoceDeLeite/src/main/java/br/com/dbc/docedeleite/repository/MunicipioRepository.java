/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.repository;

import br.com.dbc.docedeleite.entity.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author gustavo.comaru
 */
public interface MunicipioRepository extends JpaRepository<Municipio, Long>{
    
    public Municipio findByNomeIBGE(String nome);
}
