/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.service;

import br.com.dbc.docedeleite.entity.ControleProcesso;
import br.com.dbc.docedeleite.repository.ControleProcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author jonas
 */
@Service
public class ControleProcessoService extends AbstractCrudService<ControleProcesso> {

    @Autowired
    private ControleProcessoRepository controleProcessoRepository;
    
    @Override
    public JpaRepository<ControleProcesso, Long> getRepository() {
        return controleProcessoRepository;
    }
    
}
