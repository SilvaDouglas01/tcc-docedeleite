package br.com.dbc.docedeleite.entity;

import javax.persistence.*;

/**
 * Created by nydiarra on 06/05/17.
 */
@Entity
@Table(name="app_role")
@SequenceGenerator( name = "APP_ROLE_SEQ", sequenceName = "APP_ROLE_SEQ", allocationSize = 1)
public class Role {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "APP_ROLE_SEQ" )
    private Long id;

    @Column(name="role_name")
    private String roleName;

    @Column(name="description")
    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
