/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import lombok.Builder;
import lombok.Data;

/**
 *
 * @author jonas.borges
 */
@Builder
@Data
public class DateFormatter {
    
    public LocalDate stringToMesExtrato(String mesExtrato){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate mesExtratoFormatado = LocalDate.parse("01/" + mesExtrato, dtf);
        return mesExtratoFormatado;
    }
}
