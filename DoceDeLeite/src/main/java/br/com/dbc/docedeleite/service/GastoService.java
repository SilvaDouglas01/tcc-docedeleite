/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.service;

import br.com.dbc.docedeleite.dto.ListaEstabelecimentoDTO;
import br.com.dbc.docedeleite.dto.ListaGrupoDTO;
import br.com.dbc.docedeleite.dto.MaiorGastoDTO;
import br.com.dbc.docedeleite.dto.SomaDTO;
import br.com.dbc.docedeleite.dto.ValorEstadoDTO;
import br.com.dbc.docedeleite.entity.ControleProcesso;
import br.com.dbc.docedeleite.rest.dto.GastoDTO;
import br.com.dbc.docedeleite.entity.Gasto;
import br.com.dbc.docedeleite.enums.Status;
import br.com.dbc.docedeleite.repository.GastoRepository;
import br.com.dbc.docedeleite.utils.DateFormatter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author gustavo.comaru
 */
@Service
public class GastoService extends AbstractCrudService<Gasto> {

    @Autowired
    private GastoRepository repository;

    @Override
    public JpaRepository getRepository() {
        return repository;
    }

    @Autowired
    private EstabelecimentoService estabelecimentoService;

    @Autowired
    private MunicipioService municipioService;

    @Autowired
    private OrgaoMaximoService orgaoMaximoService;

    @Autowired
    private ControleProcessoService controleProcessoService;
    
    @Autowired
    private EstadoService estadoService;

    @Retryable(maxAttempts = 7, backoff = @Backoff(50000))
    @Async
    public CompletableFuture<List<Gasto>> buscarTransacoes(int page, ControleProcesso controle) {
        List<Gasto> gastos = new ArrayList<>();
        try {
            String apiUrl = "http://www.transparencia.gov.br/api-de-dados/cartoes?mesExtratoInicio=01/2018&mesExtratoFim=12/2018&pagina=" + page;
            RestTemplate rt = new RestTemplate();
            ResponseEntity<GastoDTO[]> res = rt.getForEntity(apiUrl, GastoDTO[].class);
            List<GastoDTO> gastoList = Arrays.asList(res.getBody());
            gastos = salvarDeListaDTO(gastoList);
            System.out.println("Entrou :" + page);
        } catch (Exception e) {
            controle.setStatus(Status.FALHA);
            controleProcessoService.save(controle);
        }
        return CompletableFuture.completedFuture(gastos);
    }

    public List<Gasto> salvarDeListaDTO(List<GastoDTO> gastosDTO) {
        List<Gasto> gastos = new ArrayList<>();

        for (GastoDTO g : gastosDTO) {
            try {

                Gasto gastoVerificador = repository.findByIdGastoRest(g.getId());
                if (gastoVerificador == null) {
                    Gasto gasto = repository.save(
                            Gasto.builder()
                                    .mesExtrato(DateFormatter.builder().build().stringToMesExtrato(g.getMesExtrato()))
                                    .idGastoRest(g.getId())
                                    .valorTransacao(Double.parseDouble(g.getValorTransacao().replaceAll(",", ".")))
                                    .dataTransacao(g.getDataTransacao())
                                    .estabelecimento(estabelecimentoService.salvarDeDTO(g.getEstabelecimento()))
                                    .municipio(municipioService.salvarDeDTO(g.getEstabelecimento().getMunicipio()))
                                    .orgaoMaximo(orgaoMaximoService.salvarDeDTO(g.getUnidadeGestora().getOrgaoVinculado()
                                            .getOrgaoMaximo())).build());
                    gastos.add(gasto);
                }

            } catch (Exception e) {

                Gasto gastoVerificador = repository.findByIdGastoRest(g.getId());

                if (gastoVerificador == null) {

                    g.setDataTransacao(null);

                    Gasto gasto = repository.save(
                            Gasto.builder()
                                    .mesExtrato(DateFormatter.builder().build().stringToMesExtrato(g.getMesExtrato()))
                                    .valorTransacao(Double.parseDouble(g.getValorTransacao().replaceAll(",", ".")))
                                    .dataTransacao(g.getDataTransacao())
                                    .estabelecimento(estabelecimentoService.salvarDeDTO(g.getEstabelecimento()))
                                    .municipio(municipioService.salvarDeDTO(g.getEstabelecimento().getMunicipio()))
                                    .orgaoMaximo(orgaoMaximoService.salvarDeDTO(g.getUnidadeGestora().getOrgaoVinculado()
                                            .getOrgaoMaximo())).build());
                    gastos.add(gasto);
                }

            }
        }

        return gastos;
    }

    public List<ValorEstadoDTO> somaPorEstado(String ini, String fim) {
        List<ValorEstadoDTO> list = estadoService.getSiglaEValorTotal("01/" + ini, "01/" + fim);
        return list;
    }

    public SomaDTO somaPeriodo(String ini, String fim) {
        LocalDate inicio = DateFormatter.builder().build().stringToMesExtrato(ini);
        LocalDate fin = DateFormatter.builder().build().stringToMesExtrato(fim);
        return SomaDTO.builder()
                .soma(repository.sumValorByLocalDateBetween(inicio, fin))
                .build();
    }

    public SomaDTO mediaPeriodo(String ini, String fim) {
        LocalDate inicio = DateFormatter.builder().build().stringToMesExtrato(ini);
        LocalDate fin = DateFormatter.builder().build().stringToMesExtrato(fim);
        return SomaDTO.builder()
                .soma(repository.sumValorByLocalDateBetween(inicio, fin) / repository.countByMesExtratoBetween(inicio, fin))
                .build();
    }

    public List<ListaGrupoDTO> paginar(String inicio, String fim) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate ini = LocalDate.parse("01/" + inicio + "/2018", dtf);
        LocalDate fin = LocalDate.parse("01/" + fim + "/2018", dtf);

        List<Gasto> transacoes = repository.findByMesExtratoBetween(ini, fin);

        List<ListaEstabelecimentoDTO> estabelecimentos = new ArrayList<ListaEstabelecimentoDTO>();
        for (Gasto transacao : transacoes) {
            boolean naoExiste = true;
            for (ListaEstabelecimentoDTO dto : estabelecimentos) {
                if (transacao.getEstabelecimento().getNome() == dto.getNome()) {
                    dto.setValor(dto.getValor() + transacao.getValorTransacao());
                    naoExiste = false;
                    break;
                }
            }
            if (naoExiste) {
                estabelecimentos.add(ListaEstabelecimentoDTO
                        .builder()
                        .nome(transacao.getEstabelecimento().getNome())
                        .valor(transacao.getValorTransacao())
                        .nomeGrupo(transacao.getEstabelecimento().getGrupo().getNome())
                        .build()
                );
            }
        }

        List<ListaGrupoDTO> grupos = new ArrayList<ListaGrupoDTO>();
        for (ListaEstabelecimentoDTO estabelecimento : estabelecimentos) {
            boolean naoExiste = true;
            for (ListaGrupoDTO grupo : grupos) {
                if (estabelecimento.getNomeGrupo() == grupo.getNome()) {
                    grupo.setValor(grupo.getValor() + estabelecimento.getValor());
                    List<ListaEstabelecimentoDTO> listaTemp = grupo.getEstabelecimentos();
                    listaTemp.add(estabelecimento);
                    grupo.setEstabelecimentos(listaTemp);
                    naoExiste = false;
                    break;
                }
            }
            if (naoExiste) {
                grupos.add(ListaGrupoDTO
                        .builder()
                        .nome(estabelecimento.getNomeGrupo())
                        .valor(estabelecimento.getValor())
                        .estabelecimentos(Arrays.asList(estabelecimento))
                        .build()
                );
            }
        }

        return grupos;
    }

    public MaiorGastoDTO getMaiorGastoPeriodo(String ini, String fim) {
        LocalDate inicio = DateFormatter.builder().build().stringToMesExtrato(ini);
        LocalDate fin = DateFormatter.builder().build().stringToMesExtrato(fim);
        Gasto maiorGasto = repository.findTopByMesExtratoBetweenOrderByValorTransacaoDesc(inicio, fin);
        return MaiorGastoDTO.builder()
                .estabelecimento(maiorGasto.getEstabelecimento().getNome())
                .valor(maiorGasto.getValorTransacao())
                .build();
    }

}
