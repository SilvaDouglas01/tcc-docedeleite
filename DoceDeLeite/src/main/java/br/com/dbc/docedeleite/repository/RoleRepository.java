package br.com.dbc.docedeleite.repository;


import br.com.dbc.docedeleite.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by nydiarra on 06/05/17.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByRoleName(String roleName);
}
