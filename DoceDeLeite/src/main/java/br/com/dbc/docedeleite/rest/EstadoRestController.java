/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import br.com.dbc.docedeleite.entity.Estado;
import br.com.dbc.docedeleite.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas
 */
@RestController
@RequestMapping("/api/estado")
public class EstadoRestController extends AbstractRestController<Estado, EstadoService> {
    
    @Autowired
    private EstadoService estadoService;

    @Override
    protected EstadoService getService() {
        return estadoService;
    }
    
}
