/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.service;

import br.com.dbc.docedeleite.dto.GrupoTabelaDTO;
import br.com.dbc.docedeleite.dto.SomaDTO;
import br.com.dbc.docedeleite.rest.dto.CnaeDTO;
import br.com.dbc.docedeleite.entity.Grupo;
import br.com.dbc.docedeleite.repository.GrupoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Service;

@Service
public class GrupoService extends AbstractCrudService<Grupo> {

    @Autowired
    private GrupoRepository repository;
    
    @Override
    public GrupoRepository getRepository() {
        return repository;
    }

    public Grupo salvarDeDTO(CnaeDTO dto) {

        Grupo grupo = repository.findByNome(dto.getGrupo());

        if (grupo == null) {
            grupo = repository.save(Grupo
                    .builder()
                    .nome(dto.getGrupo())
                    .build());
        }
        
        return grupo;
    }
    
    public PagedListHolder getTabelasGrupos(Long pagina, Long tamanho, String dataIni, String dataFim){
        
        dataIni = "01/" + dataIni;
        dataFim = "01/" + dataFim;
        
        List<GrupoTabelaDTO> grupos = repository.valorTotal(dataIni, dataFim);
        
        PagedListHolder page = new PagedListHolder(grupos);
        
        page.setPageSize(tamanho.intValue());
        page.setPage(pagina.intValue()); 
        
        return page;
    }
    
    public SomaDTO getPageCountTabelaGrupo(String dataIni, String dataFim){
        return SomaDTO.builder()
                .soma(getTabelasGrupos(0l, 20l, dataIni, dataFim).getPageCount()*1.0)
                .build();
    }

}
