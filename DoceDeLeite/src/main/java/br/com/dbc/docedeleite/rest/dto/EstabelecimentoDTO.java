
package br.com.dbc.docedeleite.rest.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cnae",
    "codigoFormatado",
    "complementoEndereco",
    "dataAbertura",
    "descricaoLogradouro",
    "enderecoEletronico",
    "localidadePessoa",
    "municipio",
    "naturezaJuridica",
    "nome",
    "nomeBairro",
    "nomeFantasiaReceita",
    "numeroCEP",
    "numeroEndereco",
    "numeroInscricaoSocial",
    "numeroTelefone",
    "razaoSocialReceita",
    "tipoCodigo",
    "tipoPessoa"
})
public class EstabelecimentoDTO {

    @JsonProperty("cnae")
    private CnaeDTO cnae;
    @JsonProperty("codigoFormatado")
    private String codigoFormatado;
    @JsonProperty("complementoEndereco")
    private String complementoEndereco;
    @JsonProperty("dataAbertura")
    private String dataAbertura;
    @JsonProperty("descricaoLogradouro")
    private String descricaoLogradouro;
    @JsonProperty("enderecoEletronico")
    private String enderecoEletronico;
    @JsonProperty("localidadePessoa")
    private String localidadePessoa;
    @JsonProperty("municipio")
    private MunicipioDTO municipio;
    @JsonProperty("naturezaJuridica")
    private NaturezaJuridicaDTO naturezaJuridica;
    @JsonProperty("nome")
    private String nome;
    @JsonProperty("nomeBairro")
    private String nomeBairro;
    @JsonProperty("nomeFantasiaReceita")
    private String nomeFantasiaReceita;
    @JsonProperty("numeroCEP")
    private String numeroCEP;
    @JsonProperty("numeroEndereco")
    private String numeroEndereco;
    @JsonProperty("numeroInscricaoSocial")
    private String numeroInscricaoSocial;
    @JsonProperty("numeroTelefone")
    private String numeroTelefone;
    @JsonProperty("razaoSocialReceita")
    private String razaoSocialReceita;
    @JsonProperty("tipoCodigo")
    private String tipoCodigo;
    @JsonProperty("tipoPessoa")
    private String tipoPessoa;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cnae")
    public CnaeDTO getCnae() {
        return cnae;
    }

    @JsonProperty("cnae")
    public void setCnae(CnaeDTO cnae) {
        this.cnae = cnae;
    }

    @JsonProperty("codigoFormatado")
    public String getCodigoFormatado() {
        return codigoFormatado;
    }

    @JsonProperty("codigoFormatado")
    public void setCodigoFormatado(String codigoFormatado) {
        this.codigoFormatado = codigoFormatado;
    }

    @JsonProperty("complementoEndereco")
    public String getComplementoEndereco() {
        return complementoEndereco;
    }

    @JsonProperty("complementoEndereco")
    public void setComplementoEndereco(String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }

    @JsonProperty("dataAbertura")
    public String getDataAbertura() {
        return dataAbertura;
    }

    @JsonProperty("dataAbertura")
    public void setDataAbertura(String dataAbertura) {
        this.dataAbertura = dataAbertura;
    }

    @JsonProperty("descricaoLogradouro")
    public String getDescricaoLogradouro() {
        return descricaoLogradouro;
    }

    @JsonProperty("descricaoLogradouro")
    public void setDescricaoLogradouro(String descricaoLogradouro) {
        this.descricaoLogradouro = descricaoLogradouro;
    }

    @JsonProperty("enderecoEletronico")
    public String getEnderecoEletronico() {
        return enderecoEletronico;
    }

    @JsonProperty("enderecoEletronico")
    public void setEnderecoEletronico(String enderecoEletronico) {
        this.enderecoEletronico = enderecoEletronico;
    }

    @JsonProperty("localidadePessoa")
    public String getLocalidadePessoa() {
        return localidadePessoa;
    }

    @JsonProperty("localidadePessoa")
    public void setLocalidadePessoa(String localidadePessoa) {
        this.localidadePessoa = localidadePessoa;
    }

    @JsonProperty("municipio")
    public MunicipioDTO getMunicipio() {
        return municipio;
    }

    @JsonProperty("municipio")
    public void setMunicipio(MunicipioDTO municipio) {
        this.municipio = municipio;
    }

    @JsonProperty("naturezaJuridica")
    public NaturezaJuridicaDTO getNaturezaJuridica() {
        return naturezaJuridica;
    }

    @JsonProperty("naturezaJuridica")
    public void setNaturezaJuridica(NaturezaJuridicaDTO naturezaJuridica) {
        this.naturezaJuridica = naturezaJuridica;
    }

    @JsonProperty("nome")
    public String getNome() {
        return nome;
    }

    @JsonProperty("nome")
    public void setNome(String nome) {
        this.nome = nome;
    }

    @JsonProperty("nomeBairro")
    public String getNomeBairro() {
        return nomeBairro;
    }

    @JsonProperty("nomeBairro")
    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    @JsonProperty("nomeFantasiaReceita")
    public String getNomeFantasiaReceita() {
        return nomeFantasiaReceita;
    }

    @JsonProperty("nomeFantasiaReceita")
    public void setNomeFantasiaReceita(String nomeFantasiaReceita) {
        this.nomeFantasiaReceita = nomeFantasiaReceita;
    }

    @JsonProperty("numeroCEP")
    public String getNumeroCEP() {
        return numeroCEP;
    }

    @JsonProperty("numeroCEP")
    public void setNumeroCEP(String numeroCEP) {
        this.numeroCEP = numeroCEP;
    }

    @JsonProperty("numeroEndereco")
    public String getNumeroEndereco() {
        return numeroEndereco;
    }

    @JsonProperty("numeroEndereco")
    public void setNumeroEndereco(String numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    @JsonProperty("numeroInscricaoSocial")
    public String getNumeroInscricaoSocial() {
        return numeroInscricaoSocial;
    }

    @JsonProperty("numeroInscricaoSocial")
    public void setNumeroInscricaoSocial(String numeroInscricaoSocial) {
        this.numeroInscricaoSocial = numeroInscricaoSocial;
    }

    @JsonProperty("numeroTelefone")
    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    @JsonProperty("numeroTelefone")
    public void setNumeroTelefone(String numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }

    @JsonProperty("razaoSocialReceita")
    public String getRazaoSocialReceita() {
        return razaoSocialReceita;
    }

    @JsonProperty("razaoSocialReceita")
    public void setRazaoSocialReceita(String razaoSocialReceita) {
        this.razaoSocialReceita = razaoSocialReceita;
    }

    @JsonProperty("tipoCodigo")
    public String getTipoCodigo() {
        return tipoCodigo;
    }

    @JsonProperty("tipoCodigo")
    public void setTipoCodigo(String tipoCodigo) {
        this.tipoCodigo = tipoCodigo;
    }

    @JsonProperty("tipoPessoa")
    public String getTipoPessoa() {
        return tipoPessoa;
    }

    @JsonProperty("tipoPessoa")
    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
