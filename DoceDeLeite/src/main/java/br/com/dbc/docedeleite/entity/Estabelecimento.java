/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.entity;

import br.com.dbc.docedeleite.dto.EstabelecimentoValorDTO;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author douglas
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ESTABELECIMENTO")
@SequenceGenerator(name = "ESTABELECIMENTO_SEQ", sequenceName = "ESTABELECIMENTO_SEQ", allocationSize = 1)

@SqlResultSetMapping(
        name = "getEstabResult",
        classes = {
            @ConstructorResult(
                    targetClass = EstabelecimentoValorDTO.class,
                    columns = {
                        @ColumnResult(name = "nome", type = String.class)
                        ,
                               @ColumnResult(name = "valor", type = Double.class)
                    }
            )
        })
@NamedNativeQuery(
        name = "Estabelecimento.valorTotalEstabelecimento",
        query = "select estabelecimento.nome, sum(gasto.valor_transacao) as valor\n"
        + "from estabelecimento \n"
        + "inner join gasto on gasto.ID_ESTABELECIMENTO = estabelecimento.id \n"
        + "inner join grupo on estabelecimento.ID_GRUPO = grupo.id \n"
        + "where grupo.nome = :nome \n"
        + "and gasto.MES_EXTRATO between :inicio and :fim \n"
        + "group by estabelecimento.nome order by valor desc",
        resultSetMapping = "getEstabResult"
)
public class Estabelecimento extends AbstractEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESTABELECIMENTO_SEQ")
    private Long id;

    @NotNull
    @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID")
    @ManyToOne
    private Grupo grupo;

    @Column
    @NotNull
    private String nome;

}
