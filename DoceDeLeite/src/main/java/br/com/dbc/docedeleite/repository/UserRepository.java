package br.com.dbc.docedeleite.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import br.com.dbc.docedeleite.entity.User;

/**
 * Created by nydiarra on 06/05/17.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    
    User findByEmail(String email);
}
