/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.repository;

import br.com.dbc.docedeleite.dto.GraficoBolhasDTO;
import br.com.dbc.docedeleite.entity.OrgaoMaximo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author gustavo.comaru
 */
public interface OrgaoMaximoRepository extends JpaRepository<OrgaoMaximo, Long> {

    public OrgaoMaximo findByNome(String nome);

    @Query(nativeQuery = true)
    public List<GraficoBolhasDTO> findValorTransferenciaByOrgaoMaximoNome(@Param("ini") String ini, @Param("fim") String fim);
}
