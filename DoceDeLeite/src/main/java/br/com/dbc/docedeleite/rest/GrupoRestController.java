/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.rest;

import br.com.dbc.docedeleite.entity.Grupo;
import br.com.dbc.docedeleite.service.GrupoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jonas
 */
@Controller
@RequestMapping("/api/grupo")
public class GrupoRestController extends AbstractRestController<Grupo, GrupoService>{
    
    @Autowired
    private GrupoService grupoService;
    
    @Override
    protected GrupoService getService() {
        return grupoService;
    }
    
    
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @GetMapping("/tabela/{pagina}/{tamanho}")
    public ResponseEntity<?> getTabelasGrupo(@PathVariable Long pagina,
            @PathVariable Long tamanho, String dataInicio, String dataFim){
        return ResponseEntity.ok(getService().getTabelasGrupos(pagina, tamanho, dataInicio, dataFim).getPageList());
    }
    
    
    @PreAuthorize("hasAuthority('ADMIN_USER') or hasAuthority('STANDARD_USER')")
    @GetMapping("/tamanho/tabela")
    public ResponseEntity<?> getPageCountTabelasGrupo(String dataInicio, String dataFim){
        return ResponseEntity.ok(getService().getPageCountTabelaGrupo(dataInicio, dataFim));
    }
}
