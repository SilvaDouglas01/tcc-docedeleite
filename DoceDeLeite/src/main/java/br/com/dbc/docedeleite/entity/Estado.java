/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.docedeleite.entity;

import br.com.dbc.docedeleite.dto.ValorEstadoDTO;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author jonas.borges
 */
@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ESTADO")
@SequenceGenerator(name = "ESTADOO_SEQ", sequenceName = "ESTADOO_SEQ", allocationSize = 1)
@SqlResultSetMapping(
        name = "getEstadoResult",
        classes = {
            @ConstructorResult(
                    targetClass = ValorEstadoDTO.class,
                    columns = {
                        @ColumnResult(name = "sigla", type = String.class)
                        ,
                               @ColumnResult(name = "valor", type = Double.class)
                    }
            )
        })
@NamedNativeQuery(
        name = "Estado.getSiglaEValorTotal",
        query = "select e.sigla, sum(g.valor_transacao) as valor \n"
        + "from gasto g \n"
        + "join municipio on g.id_municipio = municipio.id\n"
        + "join estado e on municipio.id_estado = e.id\n"
        + "where g.mes_extrato between :ini and :fim\n"
        + "group by e.sigla",
        resultSetMapping = "getEstadoResult"
)
public class Estado extends AbstractEntity implements Serializable {

    @Id
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ESTADOO_SEQ")
    private Long id;

    @NotNull
    @Column(name = "NOME")
    @Size(min = 1, max = 100)
    private String nome;

    @NotNull
    @Column(name = "SIGLA")
    @Size(min = 1, max = 10)
    private String sigla;
}
