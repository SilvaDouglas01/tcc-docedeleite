export default class Api {
  constructor( url ) {
    this.url = url
}


async verificarToken() {
  if(localStorage.getItem('authorization') === null)
    return '401'
  return new Promise( resolve => {
    fetch( this.url + "api/gastos/token", this.getHeaders() )
      .then( j => {
        resolve(j.status)
    } )
  })
}

async getUser() {
  return new Promise( resolve => {
    fetch( this.url + "api/user/details", this.getHeaders() )
      .then( j => j.json() )
      .then( t => {
        const user = t 
        resolve( user )
      } )
  } )
}

async getGrupos(pagina, tamanho, inicio, fim) {
  return new Promise( resolve => {
    fetch( this.url + "api/grupo/tabela/"+pagina+"/"+tamanho+"?dataInicio="+inicio+"&dataFim="+fim, this.getHeaders())
      .then( j => j.json() )
      .then( t => {
        const grupos = t 
        grupos.forEach(grupo => {
          grupo.valor = this.numberToReal(grupo.valor)    
        });
        resolve( grupos )
      } )
    } )
}

async getEstabelecimentos(inicio, fim, grupo) {
  return new Promise( resolve => {
    fetch( this.url + "api/estabelecimento/valorTotal?nome="+grupo+"&dataInicio="+inicio+"&dataFim="+fim, this.getHeaders())
      .then( j => j.json() )
      .then( t => {
        const estabelecimentos = t 
        estabelecimentos.forEach(estabelecimento => {
          estabelecimento.valor = this.numberToReal(estabelecimento.valor)    
        });
        resolve( estabelecimentos )
      } )
    } )
}

async getGruposPageSize(inicio, fim) {
  return new Promise( resolve => {
    fetch( this.url + "api/grupo/tamanho/tabela?dataInicio="+inicio+"&dataFim="+fim, this.getHeaders())
      .then( j => j.json() )
      .then( t => {
        const paginas = t.soma 
        resolve(paginas)
      } )
    } )
}

async getMaiorGasto(inicio, fim) {
  return new Promise( resolve => {
    fetch( this.url + "api/gastos/maior?inicio="+inicio+"&fim="+fim, this.getHeaders())
      .then( j => j.json() )
      .then( t => {
        const gasto = t 
        gasto.valor = this.numberToReal(gasto.valor)
        resolve(gasto)
      } )
    } )
}

async getSomaPeriodo(inicio, fim) {
  return new Promise( resolve => {
    fetch( this.url + "api/gastos/somaperiodo?inicio="+inicio+"&fim="+fim, this.getHeaders() )
      .then( j => j.json() )
      .then( t => {
        const soma = this.numberToReal(t.soma) 
        resolve( soma )
      } )
    } )
}


numberToReal(numero) {
  numero = numero.toFixed(2).split('.');
  numero[0] = "R$ " + numero[0].split(/(?=(?:...)*$)/).join('.');
  return numero.join(',');
}

getHeaders(){
  var myHeaders = new Headers();

  myHeaders.append('Authorization', 'bearer '+localStorage.getItem('authorization').replace(/['"]+/g, ''));
  var myInit = { method: 'GET',
               headers: myHeaders,
               mode: 'cors',
               cache: 'default'};
  return myInit
}

async getMediaPeriodo(inicio, fim) {
  return new Promise( resolve => {
    fetch( this.url + "api/gastos/mediaperiodo?inicio="+inicio+"&fim="+fim, this.getHeaders())
      .then( j => j.json() )
      .then( t => {
        const media = this.numberToReal(t.soma) 
        resolve( media )
      } )
    } )
}

async getGraficoBolhas(dataIni, dataFim) {
  return new Promise( resolve => {
    fetch( this.url + "api/orgaos/graficobolhas?dataInicio="+ dataIni +"&dataFim=" + dataFim, this.getHeaders())
      .then( j => j.json() )
      .then( t => {
        const data = t 
        resolve(data);
      } )
    } )
}
}
