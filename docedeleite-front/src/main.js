import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Login from "./components/screens/Login.vue"
import Cadastro from "./components/screens/Cadastro.vue"
import Dashboard from "./components/screens/Dashboard.vue"
import VeeValidate from 'vee-validate'
import msgBR from 'vee-validate/dist/locale/pt_BR'
import VueResource from 'vue-resource'


Vue.config.productionTip = false
Vue.use(VueResource)

Vue.use(VeeValidate, {
  locale: 'pt_BR',
  dictionary: { 
      pt_BR: { messages: msgBR }
  }
})

Vue.use(VueRouter)
Vue.config.productionTip = false

const routes = [ 
  { name: 'login', path: '/', component: Login },
  { name: 'cadastro', path: '/cadastro', component: Cadastro },
  { name: 'dashboard', path: '/dashboard', component: Dashboard }
]

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
